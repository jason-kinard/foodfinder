package com.ironyard.repo;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ironyard.springboot.data.Vendor;
import com.ironyard.springboot.data.VendorType;

public interface VendorRepository extends PagingAndSortingRepository<Vendor, Long>{
	List<Vendor> findByName(String name);
	List<Vendor> findByType(VendorType type);
}

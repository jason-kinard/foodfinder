package com.ironyard.springboot.data;

public enum VendorType {
	VEGAN, ITALIAN, BAR, JAPANESE, FRIED_CHICKEN, BURGERS, INDIAN, DONUT_SHOP, ICE_CREAM
}
